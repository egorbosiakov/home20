(() => {
  const newUser = async (getUser) => {
    try {
      await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: new FormData(getUser),
      });
      getUser.reset();
    } catch (err) {
      console.log(упс);
    }
  };

  document.getElementById("submit").onclick = (e) => {
    const first = document.getElementsByName("firstName")[0].value;
    const last = document.getElementsByName("lastName")[0].value;
    e.preventDefault();
    newUser(document.getElementById("newForm"));
    alert(`${first} ${last}`);
  };
})();
